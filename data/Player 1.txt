Field:
-Scout (7)

Trade Deck:
-Command Ship
-Central Office
-Flagship
-Port of Call
-Defense Center
-Trade Escort
-Barter World (2)
-Freighter (2)
-Embassy Yacht (2)
-Trading Post (2)
-Cutter (3)
-Federation Shuttle (3)
-Brain World
-Machine Base
-Junkyard
-Missile Mech
-Battle Mech
-Mech World
-Patrol Mech (2)
-Stealth Needle
-Battle Station (2)
-Supply Bot (3)
-Missile Bot (3)
-Trade Bot (3)