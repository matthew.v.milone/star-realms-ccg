This file contains instrucitons for playing Star Realms CCG. It is a fan-made variant of White Wizard's Star Realms Deck-Building Game in the form of a pass-and-play app, made using Python 3. If you're unfamiliar with the original game, then follow this link to learn more:

https://www.starrealms.com/learn-to-play/

If you don't have Python installed on your computer or you just want to run the game, download the .exe file that begins with "srccg_setup".

**Gameplay Overview**

This version makes one major change to the original game: instead of sharing a trade deck, players construct their own decks before the game begins, following some restrictions that are given below.

The trade row consists of eight slots, rather than the usual five. When a card would be drawn from the trade deck (e.g. following a card acquisition), the card is drawn from the first player's trade deck if the missing card is in one of the first four slots, and the second player's deck if it's in one of the last four slots. Players can purchase from any slot, but there is a Trade penalty for cards acquired from the opponent's side.

**Constructing a Deck**

There are a few restrictions on the players' trade decks:
1. They must have exactly 40 cards.
2. The number of copies of a given card is limited by the number of copies that are in one full set of Star Realms cards.
3. The number of high-cost cards is limited. Decks cannot have more than three cards of cost 8 or above, six cards of cost 7 or above, nine cards of cost 6 or above, or twelve cards of cost 5 or above.

To modify the players' decks, select the "Manage Decks" option on the main menu. Sample decks are included. To include a card in your deck, begin a line of the text file with a hyphen, and then type the full card name. Including multiple copies of a card can be done by typing the same entry again, or by putting the number of copies in parenthesis after the card name. If you wish for your name to be visible in the game, begin a line with "Name:", and then type your name. All other lines are ignored by the program and can be used to annotate your deck.

**Using the Application**

After installing, double click on the desktop icon. The main menu should appear shortly, from which the player can edit decks or begin a game.

If the "Play Game" option is clicked, then a game window will appear. The current player's information and the Explorer pile are displayed at the bottom of the screen, the opposing player's information is at the top, and the trade row is in the middle.

Cards in the blue region are in the player's hand, while cards in red regions are play. To play a card, left-click on it. If the card is a ship, its primary ability will be played. If it is a base, then its primary ability will only be played if it consists solely of abilities which provide Combat, Trade, or Authority and no user interaction. To use a base's primary ability as it's played, press the middle mouse button. Left-clicking a card that is already in play will use an unused ability. If multiple abilities are available, then the player will be prompted to select an ability.

Right-clicking a card, whether it's in the player's hand, the trade row, or elsewhere, will create a window containing a more detailed image of the card, as well as icons representing the card's unused abilities if the card is in the player's hand or field.

To purchase a card in the trade row or destroy an opponent's base, left-click on it. At the end of each turn, remaining Combat is automatically applied to outposts, and then the opponent's face.

Sometimes, abilities will require the user to select one or more cards. Such an ability will create a window containing eligible cards. Select or deselect cards via left-clicking.

**Other Things to Know**

Cards that scrap from the trade row can be used on any slot. "Acquire" abilities, such as Blob Carrier's, negate a card's listed cost, but still require the player to pay a fee if it was acquired from the opponent's side of the trade row.

Rapid construction abilities, such as the Freighter's, overwrite previous rapid construction cards, in contrast to an official ruling. Also, cards such as Stealth Needle do count toward Blob World.

The list of supported cards includes those found in Core Set, Colony Wars, Promo Pack 1, Mercenaries, Bases & Battleshipts, Fleets & Fortresses, Crisis: Heroes, United: Heroes, United: Assault, United: Command, the Docking cards, and The Colossus, for good measure. Details on most of these cards can be found on the official card gallery. Even though it contains a few errors, it's still very useful for constructing decks:

https://www.starrealms.com/card-gallery/

I plan to gradually expand the roster of supported cards. For most of these cards, the limiting factor is the lack of good-quality images. I also plan to improve the UI, improve the menues, and enable peer-to-peer online play. I'll consider making changes for balance purposes if it proves necessary.

If you discover a bug, please leave a post on the Board Game Geek forum detailing the problem.

**Final Note**

In the many, many hours of creating and testing Star Realms CCG, it has never caused any sort of detrimental effect on my computer. However, if running this program somehow detrimentally effects your computer, then I am not liable for damages. You're the one who downloaded and ran an executable file from a stranger on the Internet.

That said, I stake my reputation on this app operating properly. I hope you have fun with it, and I'm interested to see what the community does with it.

--- Matthew V. Milone