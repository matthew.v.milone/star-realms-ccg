## Star Realms CCG
## Version 2.3.1
##
## This class is responsible for handling requests for the user to chose
## cards or abilities from a list.
##
## @author: Matthew Milone


from ..view.SingletonWindow import SingletonWindow
from ..view.RowWindow import RowWindow


class SelectionController(object):
    def __init__(self, gameWindow, DEBUG):
        self.gameWindow = gameWindow
        self.DEBUG = DEBUG
        self.window = None
        self.numToSelect = None
        self.forced = None
        self.choices = []

    ## Returns a list of two-tuples. Each tuple contains the name of the
    ## list that the card is in and its index in that list.
    ## Note that this function deactiveates the loop.
    def rowSelection(self, locationDict, instructions, numToSelect, forced):
        self.choices = []
        if any(len(vals) for vals in locationDict.values()):
            self.numToSelect, self.forced = numToSelect, forced
            if forced:
                mini = numToSelect
            else:
                mini = 0
            self.window = RowWindow(self.gameWindow, self, locationDict, instructions, mini, numToSelect)
            self.window.grab_set()
            self.window.protocol("WM_DELETE_WINDOW", lambda: None)
            self.gameWindow.enterLoop()
        return self.choices

    ## Returns a sub-list of <options>.
    ## Note that this function deactiveates the loop.
    def abilitySelection(self, card, options, numToSelect=1, forced=True):
        self.choices = []
        self.numToSelect, self.forced = numToSelect, forced
        if forced:
            mini = numToSelect
        else:
            mini = 0
        self.window = SingletonWindow(self, card, options, mini, numToSelect)
        self.window.grab_set()
        self.window.protocol("WM_DELETE_WINDOW", lambda: None)
        self.gameWindow.enterLoop()
        return self.choices

    def getNumSelected(self):
        return len(self.choices)

    def selectCard(self, listName, index):
        self.choices.append((listName, index))
        if self.getNumSelected() == self.numToSelect and self.forced:
            self.__finish__()

    def deselectCard(self, listName, index):
        self.choices.remove((listName, index))

    def selectAbility(self, symbol):
        self.choices.append(symbol)
        if self.getNumSelected() == self.numToSelect and self.forced:
            self.__finish__()

    def deselectAbility(self, symbol):
        self.choices.remove(symbol)

    def checkDone(self):
        if self.getNumSelected() > self.numToSelect:
            self.gameWindow.notify("Too many options are selected.")
        elif self.getNumSelected() < self.numToSelect and self.forced:
            self.gameWindow.notify("Too few options are selected.")
        else:
            self.__finish__()

    def showDetail(self, card, window):
        self.gameWindow.showDetail(card, middleWindow=window)

    def __finish__(self):
        self.window.destroy()
        self.gameWindow.exitLoop()