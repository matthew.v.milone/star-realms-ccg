## Star Realms CCG
## Version 2.3.1
##
## Stores functions that are called when card abilities are used.
##
## @author: Matthew Milone


from ..common.helper import moveCards, drawCards, numMatches
from ..common.constants import TRADE_ROW_LEN, TYPE_DICT, DEFAULT_DST


class AbilityHandler(object):
    def __init__(self, gameWindow, turnProcessor, selectionController, DEBUG):
        self.gameWindow = gameWindow
        self.selector = selectionController
        self.controller = turnProcessor
        self.tradeRow = turnProcessor.tradeRow
        self.cp = turnProcessor.cp
        self.op = turnProcessor.op
        self.DEBUG = DEBUG
        
        self.activeCard = None
        self.factionBonuses = ""
        self.destinations = {"S": "-", "B": "-", "H": "P"}
        self.dstLink = ""
        self.dispatcher = {
            "+C":       self.gainCombat,
            "+T":       self.gainTrade,
            "+A":       self.gainAuthority,
            "and":      self.doAll,
            "draw":     self.draw,
            "scr":      self.scrap,
            "odis":     self.opDiscard,
            "cdis":     self.cpDiscard,
            "strafe":   self.strafe,
            "cycle":    self.cycle,
            "dstr":     self.destroyBase,
            "rtrn":     self.returnBase,
            "nxt":      self.deploy,
            "acq":      self.acquire,
            "or":       self.either,
            "loop":     self.loop,
            "rcvr":     self.recover,
            "blitz":    self.blitz,
            "dock":     self.dock,
            "if":       self.conditional,
            "played":   self.getNumPlayed,
            "inplay":   self.getNumInPlay,
            "link":     self.link,
            "watch":    self.watch,
            "salv":     self.salvage,
            "copy":     self.copy,
            "adapt":    self.adapt,
            "proc":     self.proc,
            }

### Non-Ability Methods ###

    ## Uses the given ability.
    def dispatch(self, abilityName, abilityArgs):
        return self.dispatcher[abilityName](abilityArgs)

    ## Sets the card whose ability is being used.
    def setActiveCard(self, card):
        self.activeCard = card

    ## Resets the destination for card purchases to the default values.
    def resetDst(self, typeStr="SBH"):
        for t in typeStr:
            self.destinations[t] = DEFAULT_DST[t]

    ## Removes automatic ally bonuses.
    def resetFactionBonuses(self):
        self.factionBonuses = ""

    ## Switches the current player with the opposing player.
    def swapPlayers(self):
        self.cp, self.op = self.op, self.cp

### Ability Methods ###

    def gainCombat(self, amount):
        self.cp.combat += amount
        self.gameWindow.updateStats()

    def gainTrade(self, amount):
        self.cp.trade += amount
        self.gameWindow.updateStats()

    def gainAuthority(self, amount):
        self.cp.authority += amount
        self.gameWindow.updateStats()

    def doAll(self, actionsList):
        for action in actionsList:
            self.dispatch(action[0], action[1])

    def draw(self, numToDraw):
        self.cp.draw(numToDraw)

    ## @precondition: args must be a 2-tuple or 4-tuple
    ## (<number of cards to scrap>, <hand/discard pile/trade row>,
    ## forced=False, factions=GYBR)
    def scrap(self, args):
        numToScrap, locations = args[0], args[1]
        forced, factions = False, "GYBR-"
        if len(args) == 4:
            forced, factions = args[2], args[3]
        if numToScrap == "all":
            numToScrap = TRADE_ROW_LEN

        # Determine which cards the player wants to scrap
        locationDict = {}
        if "H" in locations:
            locationDict["Hand"] = self.cp.hand
        if "D" in locations:
            locationDict["Discard Pile"] = self.cp.discPile
        if "T" in locations:
            locationDict["Trade Row"] = [self.tradeRow[i][0] for i in range(TRADE_ROW_LEN)]

        if numToScrap == 1 and not forced:
            message = "You may choose a card to scrap."
        elif not forced:
            message = f"Choose up to {numToScrap} cards to scrap."
        elif numToScrap == 1:
            message = "Choose a card to scrap."
        else:
            message = f"Choose {numToScrap} cards to scrap."
            
        selectedCardLocations = self.selector.rowSelection(locationDict, message, numToScrap, forced)
        cardsToScrap = [locationDict[listName][index] for (listName, index)\
                in selectedCardLocations]
        
        # Determine which cards can be scrapped
        scrapFromHand = []
        scrapFromDisc = []
        scrapFromTR = []
        trIndices = []
        for pair in selectedCardLocations:
            listName, index = pair[0], pair[1]
            card = locationDict[listName][index]
            canScrap = False
            if card.name != None:
                if card.faction == "":
                    if "-" in factions:
                        canScrap = True
                else:
                    for f in card.faction:
                        if f in factions:
                            canScrap = True
                            break
                if canScrap:
                    if listName == "Hand":
                        scrapFromHand.append(self.cp.hand[index])
                    elif listName == "Discard Pile":
                        scrapFromDisc.append(self.cp.discPile[index])
                    elif listName == "Trade Row":
                        trIndices.append(index)
                        scrapFromTR.append(self.tradeRow[index][0])
                else:
                    self.gameWindow.notify("The ability can only scrap cards of certain factions.")
            else:
                self.gameWindow.notify("There is no card in that slot.")

        # Scrap the cards and update the display
        if "Hand" in locationDict:
            moveCards(locationDict["Hand"], self.cp.scrapPile, scrapFromHand)
            self.gameWindow.displayHand()
            self.gameWindow.displayField()
        if "Discard Pile" in locationDict:
            moveCards(locationDict["Discard Pile"], self.cp.scrapPile, scrapFromDisc)
            self.gameWindow.updateDiscPile()
        if "Trade Row" in locationDict:
            for i in trIndices:
                drawCards(self.tradeRow[i], [], 1)
            self.controller.replaceTradeRowCards(trIndices)
            self.gameWindow.updateTradeRow()

        # Success notification
        numScrapped = len(scrapFromHand) + len(scrapFromDisc) + len(scrapFromTR)
        if numScrapped:
            cardNamesString = ", ".join([card.name for card in cardsToScrap])
            self.gameWindow.notify(self.cp.name+" scrapped "+cardNamesString+".")
        return numScrapped

    def opDiscard(self, num):
        self.op.numToDisc += num
        return num

    def cpDiscard(self, args):
        numToDisc, forced = args[0], args[1]
        locationDict = {"Hand": self.cp.hand}
        forcedStr = ""
        if not forced: forcedStr = "up to "
        pStr = ""
        if numToDisc > 1: pStr = "s"
        message = f"Discard {forcedStr}{numToDisc} card{pStr}."
        cardLocations = self.selector.rowSelection(locationDict, message, numToDisc, forced)
        if len(cardLocations):
            cardsToDisc = [self.cp.hand[index] for (listName, index) in cardLocations]
            moveCards(self.cp.hand, self.cp.discPile, cardsToDisc)
            self.gameWindow.displayHand()
            self.gameWindow.displayField()
            self.gameWindow.updateDiscPile()
        return len(cardLocations)

    def strafe(self, num):
        self.draw(num)
        self.cpDiscard((num, True))

    def cycle(self, num):
        self.link((("cdis",(num,False)), ("draw","%")))
        
    def destroyBase(self, num):
        eligibleBases = []
        if self.op.hasOutposts():
            for card in self.op.field:
                if card.isBase() and card.isO:
                    eligibleBases.append(card)
        else:
            eligibleBases = self.op.getActiveBases()
        if len(eligibleBases):
            locationDict = {"Eligible Bases": eligibleBases}
            baseLocation = self.selector.rowSelection(locationDict, "Select a base to destroy.", num, False)
            if len(baseLocation):
                index = baseLocation[0][1]
                base = eligibleBases[index]
                moveCards(self.op.field, self.op.discPile, [base])
                self.gameWindow.displayWall()
                self.gameWindow.notify(base.name+" destroyed.")
        else:
            self.gameWindow.notify(self.op.name+" has no active bases.")

    def returnBase(self, num):
        locationDict = {self.cp.name + "'s Bases": self.cp.getActiveBases()}
        if self.op.getActiveOutposts():
            locationDict[self.op.name + "'s Outposts"] = self.op.getActiveOutposts()
        else:
            locationDict[self.op.name + "'s Bases"] = self.op.getActiveBases()
        baseLocation = self.selector.rowSelection(locationDict, \
                "Select a base to return to its owner's hand.", num, False)
        if len(baseLocation):
            listName, index = baseLocation[0][0], baseLocation[0][1]
            base = locationDict[listName][index]
            base.restoreAbilities()
            if listName == self.cp.name+"'s Bases":
                moveCards(self.cp.field, self.cp.hand, [base])
                self.gameWindow.displayHand()
                self.gameWindow.displayField()
            else:
                moveCards(self.op.field, self.op.hand, [base])
                self.gameWindow.displayWall()
            self.gameWindow.notify(base.name+" returned to its owner's hand.")
        else:
            self.gameWindow.notify("There are no active bases.")

    ## "H": Hand, "P": Play, "D": Top of Deck,
    ## "S": Shuffle into Deck, "-": Discard Pile
    def deploy(self, args):
        types, dst = args[0], args[1]
        for t in types:
            if dst != "-":
                self.destinations[t] = dst
        self.dstLink = types

    ## "H": Hand, "P": Play, "D": Top of Deck,
    ## "S": Shuffle into Deck, "-": Discard Pile
    ## If there is no maximum cost of the card to be acquired, use "-".
    def acquire(self, args):
        types, maxCost, dst = args[0], args[1], args[2]
        typeStr = TYPE_DICT[types]
        locationDict = {"Trade Row": [self.tradeRow[i][0] for i in range(TRADE_ROW_LEN)]}
        if maxCost == "-":
            instruction = f"Select a {typeStr} of any cost to acquire."
        else:
            instruction = f"Select a {typeStr} of cost {maxCost} or less to acquire."
        cardLocation = self.selector.rowSelection(\
                locationDict, instruction, 1, False)
        if len(cardLocation):
            listName, index = cardLocation[0][0], cardLocation[0][1]
            card = locationDict[listName][index]
            fee = self.cp.tradeCurve[index]
            if card.name == None:
                self.gameWindow.notify("There is no card in that slot.")
            elif card.type not in types:
                self.gameWindow.notify("The activated ability cannot be used to acquire a "+TYPE_DICT[card.type]+".")
            elif maxCost != "-" and card.cost > maxCost:
                self.gameWindow.notify("The activated ability cannot be used to acquire a "\
                                  +typeStr+" of cost greater than "+str(maxCost)+".")
            elif self.cp.trade < fee:
                self.gameWindow.notify(f"Insufficient trade to pay fee of {fee}.")
            else:
                self.deploy((types, dst))
                self.controller.purchaseCard(index+1, forFree=True, forceDst=True)
                self.gameWindow.updateTradeRow(indices=[index])
                self.gameWindow.notify(card.name+" acquired.")

    def either(self, args):
        o1, o2 = args[0], args[1]
        if "+" in o1[0]:
            label_1 = o1[0]
        else:
            label_1 = "1"
        if "+" in o2[0]:
            label_2 = o2[0]
        else:
            label_2 = "2"
        card = self.activeCard
        ability = self.selector.abilitySelection(card, [label_1, label_2])[0]
        if ability == label_1:
            self.dispatch(o1[0], o1[1])
        else:
            self.dispatch(o2[0], o2[1])

    ## Repeatedly execute an ability.
    def loop(self, args):
        ability, numTimes = args[0], args[1]
        for n in range(numTimes):
            self.dispatch(ability[0], ability[1])

    ## Move a card from the discard pile to the draw pile.
    def recover(self, args):
        types, maxCost = args[0], args[1]
        eligibleCards = []
        for card in self.cp.discPile:
            if card.type in types and card.cost <= maxCost:
                eligibleCards.append(card)
        if len(eligibleCards):
            locationDict = {"Eligible Cards": eligibleCards}
            cardLocation = self.selector.rowSelection(locationDict, "Select a card to recover.", 1, False)
            if len(cardLocation):
                index = cardLocation[0][1]
                card = eligibleCards[index]
                moveCards(self.cp.discPile, self.cp.drawPile, [card])
                self.gameWindow.updateDiscPile()
        else:
            self.gameWindow.notify("There are no "+TYPE_DICT[card.type] + " eligible for recovery.")

    def blitz(self, void):
        pass

    def dock(self, void):
        pass

    ## If the <condition> is met, the <effect> ability will execute.
    def conditional(self, args):
        condition, bonus = args[0], args[1]
        getBonus = False

        allyBases = self.cp.field[:]
        allyBases.extend(self.controller.temp)
        numAllyBases = numMatches(allyBases, f="GYBR-", t="B")
        numOpntBases = numMatches(self.op.field, f="GYBR-", t="B")
        
        ## Current player has two or more bases in play... (Embassy Yacht, Defense Bot, Battle Barge)
        if condition == 1:
            if numAllyBases > 1:
                getBonus = True

        ## Opponent has one or more bases in play... (Lancer)
        elif condition == 2:
            if numOpntBases > 0:
                getBonus = True

        ## Opponent has two or more bases in play... (Obliterator)
        elif condition == 3:
            if numOpntBases > 1:
                getBonus = True

        ## Current player has three or more bases in play... (Central Station)
        elif condition == 4:
            if numAllyBases > 2:
                getBonus = True

        ## At least one base played this turn...
        ## (Breeding Site, Starbase Omega, Starmarket, Fortress Oblivion)
        elif condition == 5:
            if self.getNumPlayed(("GYBR", "B")) > 0:
                getBonus = True

        if getBonus:
            self.dispatch(bonus[0], bonus[1])

    ## Returns the number of cards with the given specifications
    ## that have been played this turn.
    def getNumPlayed(self, args):
        faction, cardType = args[0], args[1]
        variable = self.controller.numPlayed(faction, cardType)
        return variable

    ## Returns the number of cards with the given specifications
    ## that are currently in play.
    def getNumInPlay(self, args):
        faction, cardType = args[0], args[1]
        variable = self.controller.numInPlay(faction, cardType)
        return variable

    ## Uses the return value of ability1 as parameter value(s) for ability2.
    ## Use <symbol> in the second ability to indicate an intended link.
    ## Putting a number after <symbol> will multiply the value of the input.
    ## Will recurse if the first optional argument is set to True.
    ## Default <symbol> value is "%". <symbol> should be single-character.
    def link(self, args):
        ability1, ability2 = args[0], args[1]
        if len(args) == 4:
            recurse, symbol = args[2], args[3]
        else:
            recurse, symbol = False, "%"
        variable = self.dispatch(ability1[0], ability1[1])
        if variable:
            a2Name, a2Args = ability2[0], ability2[1]
            # If the second ability has one argument...
            if type(a2Args) == type(""):
                if a2Args == symbol:
                    self.dispatch(a2Name, variable)
                else:
                    factor = str(a2Args[1:])
                    self.dispatch(a2Name, variable * int(factor))
            # If the second ability has two arguments...
            elif not recurse:
                newArgsList = []
                for arg in a2Args:
                    if arg == symbol:
                        newArgsList.append(variable)
                    elif symbol == arg[0]:
                        factor = str(arg[1:])
                        newArgsList.append(variable * int(factor))
                    else:
                        newArgsList.append(arg)
                newArgsTuple = tuple(newArgsList)
                self.dispatch(a2Name, newArgsTuple)
            # If the second ability calls another ability...
            else:
                subAbilityList = []
                for subAbility in a2Args:
                    subAName, subAArgs = subAbility[0], subAbility[1]
                    # If the sub-ability has one argument...
                    if type(subAArgs) == type(""):
                        if subAArgs == symbol:
                            subAbilityList.append((subAName, variable))
                        elif symbol in subAArgs:
                            factor = str(subAArgs[1:])
                            subAbilityList.append((subAName, variable * int(factor)))
                        else:
                            subAbilityList.append((subAName, subAArgs))
                    # If the sub-ability has multiple arguments...
                    elif type(subAArgs) == type(()):
                        subArgsList = []
                        for arg in subAArgs:
                            if arg == symbol:
                                subArgsList.append(variable)
                            elif symbol == arg[0]:
                                factor = str(arg[1:])
                                subArgsList.append(variable * int(factor))
                            else:
                                subArgsList.append(arg)
                        subArgsTuple = tuple(subArgsList)
                        subAbilityList.append((subAName, subArgsTuple))
                subAbilityTuple = tuple(subAbilityList)
                self.dispatch(a2Name, subAbilityTuple)            

    ## Initiates a watch for cards of the given characteristics, causing the
    ## controller to perform the given ability whenever such a card is played.
    def watch(self, args):
        condition, effect = args[0], args[1]
        self.controller.addTrigger(condition, effect)

    ## Provides a combat bonus depending on how many cards have been scrapped.
    ## cFactor is a multiplier for the number of scrapped cards.
    def salvage(self, cFactor):
        self.gainCombat(len(self.cp.scrapPile) * cFactor)

    ## Clones a card. For avalablePlayers, "C" is the current player
    ## and "O" is the opposing player.
    def copy(self, args):
        eligibleTypes, availablePlayers, onlyNew, onlyActive = \
                       args[0], args[1], args[2], args[3]
        candidates = []
        if "C" in availablePlayers:
            candidates.extend(self.cp.field)
        if not onlyActive:
            candidates.extend(self.cp.scrapPile)
        if "O" in availablePlayers:
            candidates.extend(self.op.field)
        eligibleCards = [card for card in candidates \
                         if (not onlyNew or card in self.controller.cardsPlayed)\
                         and card.type[0] in eligibleTypes]
        locationDict = {"Eligible Cards": eligibleCards}
        instructions = "Choose a card to copy."
        cardLocation = self.selector.rowSelection(locationDict, instructions, 1, True)[0]
        listName, index = cardLocation[0], cardLocation[1]
        cardName = locationDict[listName][index].name
        self.activeCard.imitate(cardName, self.activeCard.faction)
        if self.activeCard.type == "S":
            self.controller.useAbility(self.activeCard, "P")
        else:
            self.gameWindow.displayField()

    ## Gives a card additional factions.
    def adapt(self, args):
        numExtraFactions, costPerFaction = args[0], args[1]
        if len(args) == 3:
            forced = args[2]
        else:
            forced = True
        if costPerFaction != 0:
            numExtraFactions = min(numExtraFactions, self.cp.trade//costPerFaction)
        if numExtraFactions == "all":
            self.activeCard.faction = "GYBR"
            return "GYBR"
        else:
            extraFactions = self.selector.abilitySelection(self.activeCard, ["G","Y","B","R"], numExtraFactions, forced)
            self.activeCard.faction = "".join(extraFactions)
            self.cp.trade -= len(extraFactions)*costPerFaction
            self.gameWindow.updateStats()
            return extraFactions

    ## Allows ally bonuses of the given factions to be proc'd for
    ## the remainder of the turn.
    def proc(self, factions):
        if factions == "all":
            factions = "GYBR"
        for f in factions:
            if f not in self.factionBonuses:
                self.factionBonuses = self.factionBonuses + f