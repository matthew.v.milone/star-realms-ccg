## Star Realms CCG
## Version 2.3.1
##
## Reads two custom decks specified in text files "p1.txt" and "p2.txt",
## then simulates a custom deck variant of Star Realms.
##
## If debug mode is on, then each player's initial hand, field, and discard
## pile can also be specified in the files.
##
## @author: Matthew Milone


from random import shuffle

from ..controller.AbilityHandler import AbilityHandler
from ..controller.TurnProcessor import TurnProcessor
from ..controller.SelectionController import SelectionController
from ..model.Player import Player
from ..model.Card import Card
from ..view.GameWindow import GameWindow
from ..common.helper import readPlayerData, cardsFromNames, vPrint, drawCards
from ..common.constants import P1_FILE, P2_FILE, INITIAL_AUTHORITY, TRADE_ROW_LEN, HAND_SIZE


class GameController(object):
    def __init__(self):
        pass

    def playGame(self, menu, debug):
        p1Info = readPlayerData(P1_FILE, 1, INITIAL_AUTHORITY, debug)
        p2Info = readPlayerData(P2_FILE, 2, INITIAL_AUTHORITY, debug)
        p1Name, p2Name = p1Info[0], p2Info[0]
        p1Authority, p2Authority = p1Info[1], p2Info[1]
        p1TD, p2TD = cardsFromNames(p1Info[2]), cardsFromNames(p2Info[2])
        p1Discard, p2Discard = cardsFromNames(p1Info[3]), cardsFromNames(p2Info[3])
        p1Draw, p2Draw = cardsFromNames(p1Info[4]), cardsFromNames(p2Info[4])
        p1Field, p2Field = cardsFromNames(p1Info[5]), cardsFromNames(p2Info[5])
        p1Hand, p2Hand = cardsFromNames(p1Info[6]), cardsFromNames(p2Info[6])

        tradeRow = [[] for n in range(TRADE_ROW_LEN)]

        player1 = Player(p1Name, 1, p1TD[:], p1Authority, p1Discard[:], p1Draw[:], p1Field[:], p1Hand[:], tradeRow)
        player2 = Player(p2Name, 2, p2TD[:], p2Authority, p2Discard[:], p2Draw[:], p2Field[:], p2Hand[:], tradeRow)
        if player1.name == player2.name:
            player1.name = player1.name + " (1)"
            player2.name = player2.name + " (2)"
        player1.opnt, player2.opnt = player2, player1

        if debug:
            vPrint("P1 TD", player1.tDeck)
            vPrint("P1 Disc", player1.discPile)
            vPrint("P1 Draw", player1.drawPile)
            vPrint("P1 Field", player1.field)
            vPrint("P1 Hand", player1.hand)

            vPrint("P2 TD", player2.tDeck)
            vPrint("P2 Disc", player2.discPile)
            vPrint("P2 Draw", player2.drawPile)
            vPrint("P2 Field", player2.field)
            vPrint("P2 Hand", player2.hand)
        else:
            shuffle(player1.tDeck)
            shuffle(player2.tDeck)
            shuffle(player1.drawPile)
            shuffle(player2.drawPile)

        for n in range(TRADE_ROW_LEN // 2):
            drawCards(player1.tDeck, tradeRow[n], 1)
        for n in range(TRADE_ROW_LEN - 1, TRADE_ROW_LEN // 2 - 1, -1):
            drawCards(player2.tDeck, tradeRow[n], 1)
        tradeRow.append([Card("Explorer") for n in range(10)])

        controller = TurnProcessor(tradeRow, player1, player2, debug)
        gameWindow = GameWindow(menu, tradeRow, player1, player2, controller, debug)
        selector = SelectionController(gameWindow, debug)
        handler = AbilityHandler(gameWindow, controller, selector, debug)
        controller.gameWindow, controller.handler, controller.selector = gameWindow, handler, selector

        player1.srGUI, player2.srGUI = gameWindow, gameWindow

        # Begin Game
        if not debug:
            player1.draw(HAND_SIZE - 2)
            player2.draw(HAND_SIZE)
        turn = 0
        authorityCap = 2 * INITIAL_AUTHORITY
        while player1.authority > 0 and (player1.authority < authorityCap or player2.authority < authorityCap):
            turn += 1
            controller.doTurn(player1, turn)
            if player2.authority <= 0 or player1.authority >= authorityCap and player2.authority >= authorityCap:
                break
            controller.doTurn(player2, turn)

        # End Game
        gameWindow.endGame()