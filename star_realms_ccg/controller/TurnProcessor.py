## Star Realms CCG
## Version 2.3.1
##
## Application-tier component that tracks the current player and processes
## instructions from the GUI.
##
## @author: Matthew Milone


from random import shuffle

from ..model.Card import Card
from ..common.helper import drawCards, moveCards, isAllStats, numMatches, hasAllyAbility
from ..common.constants import TRADE_ROW_LEN, HAND_SIZE, ABILITY_TYPES, C_ENUM, CARD_DATA


class TurnProcessor(object):
    def __init__(self, tradeRow, activePlayer, opposingPlayer, DEBUG):
        self.tradeRow = tradeRow
        self.cp = activePlayer
        self.op = opposingPlayer
        self.gameWindow = None
        self.handler = None
        self.selector = None
        self.DEBUG = DEBUG

        self.temp = []
        self.cardsPlayed = []
        self.triggerConditions = []
        self.triggerEffects = []
        self.continueTurn = True

    def doTurn(self, player, turn):
        self.continueTurn = True
        self.handler.resetDst()
        self.handler.resetFactionBonuses()
        self.cardsPlayed = []
        self.triggerConditions = []
        self.triggerEffects = []
        self.gameWindow.notify(player.name + ": Turn " + str(turn))
        self.gameWindow.update()
        if self.cp.numToDisc:
            self.handler.cpDiscard((min(self.cp.numToDisc, HAND_SIZE), True))
            self.cp.numToDisc = 0
        while self.continueTurn:
            self.gameWindow.enterLoop()

    def endTurn(self):
        # Discard hand.
        drawCards(self.cp.hand, self.cp.discPile, len(self.cp.hand))

        # Discard ships from field.
        cardsToDiscard = []
        dockedCards = []
        for card in self.cp.field:
            p = card.primary
            if card.type == "S":
                if p != () and (p[0] == "dock" or type(p[1]) is tuple and ("dock", None) in p[1]) and \
                        numMatches(self.cp.field[:], f=card.faction, t="B"):
                    dockedCards.append(card)
                    card.restoreAbilities()
                else:
                    cardsToDiscard.append(card)
        for card in cardsToDiscard:
            if card.name != card.trueName or card.faction != card.trueFaction:
                card.imitate(card.trueName)
        moveCards(self.cp.field, self.cp.discPile, cardsToDiscard)
        moveCards(self.cp.field, self.cp.hand, dockedCards)

        # Draw hand.
        self.cp.draw(update=False)

        # Clear scrap pile.
        self.cp.clearScrapPile()
        self.op.clearScrapPile()

        # Restore bases' abilities.
        for card in self.cp.field:
            if card.type[0] != "H":
                card.restoreAbilities()

        # Destroy outposts and deal remaining damage.
        while self.op.hasOutposts():
            eligibleBases = False
            for base in self.op.getActiveBases():
                if base.isO and self.validateDestroyBase(base):
                    self.destroyBase(base)
                    eligibleBases = True
                    break
            if not eligibleBases:
                break
        if not self.op.hasOutposts():
            self.op.authority -= self.cp.combat

        # Reset trade and combat.
        self.cp.combat = 0
        self.cp.trade = 0

        # Update player roles.
        self.cp, self.op = self.op, self.cp
        self.gameWindow.swapPlayers()
        self.handler.swapPlayers()

        # Interrupt the GUI.
        self.gameWindow.exitLoop()
        self.continueTurn = False

    def playCard(self, card):
        self.gameWindow.exitLoop()
        self.cardsPlayed.append(card)
        self.gameWindow.notify(f"{self.cp.name} played {card.name}.")

        # Execute the primary ability, if appropriate.
        if self.temp == []:
            moveCards(self.cp.hand, self.temp, [card])
        if (card.type == "S" or card.type == "H") and card.primary != () or \
                card.isBase() and isAllStats(card.primary):
            if not isAllStats(card.primary):
                self.gameWindow.displayHand()
                self.gameWindow.displayField()
            self.useAbility(self.temp[-1], "P")
        moveCards(self.temp, self.cp.field, self.temp[:])
        self.gameWindow.displayHand()
        self.gameWindow.displayField()

        # If a ____ was played, do ____.
        for condition, effect in zip(self.triggerConditions, self.triggerEffects):
            faction = condition[0]
            cardType = condition[1]
            if numMatches([card], faction, cardType) == 1:
                effectName = effect[0]
                effectArgs = effect[1]
                self.handler.dispatch(effectName, effectArgs)

        # Activate eligible stat-gaining ally abilities of all cards in field.
        for card in self.cp.field:
            eligibleAbilityTypes = []
            for aType in card.unusedAbilities:
                if aType[0] == "@" and \
                        isAllStats(card.unusedAbilities[aType]) and \
                        hasAllyAbility(card, self.cp, aType, self.handler.factionBonuses):
                    eligibleAbilityTypes.append(aType)
            for aType in eligibleAbilityTypes:
                self.useAbility(card, aType)
                self.gameWindow.updateStats()

    ## @precondition: the ability to be used must not be null--i.e. ()
    def useAbility(self, card, aType):
        ability = card.unusedAbilities[aType]
        aName, args = ability[0], ability[1]
        self.handler.setActiveCard(card)
        card.markAbility(aType)
        if aType == "P":
            self.gameWindow.notify(f"{self.cp.name} used {card.name}'s primary ability.")
        elif "@" in aType:
            self.gameWindow.notify(f"{self.cp.name} used {card.name}'s ally ability.")
        elif aType == "$":
            moveCards(self.cp.field, self.cp.scrapPile, [card])
            self.gameWindow.displayField()
            self.gameWindow.notify(f"{self.cp.name} scrapped {card.name}.")
        self.handler.dispatch(aName, args)
        self.handler.setActiveCard(None)

    ## Chooses an ability from a card's available abilities.
    ## Called by FieldButton.
    def chooseAbility(self, card):
        abilityKeys = list(card.unusedAbilities.keys())
        usableAbilities = []
        for a in ABILITY_TYPES:
            if a in abilityKeys:
                if "@" not in a or hasAllyAbility(card, self.cp, a, self.handler.factionBonuses):
                    usableAbilities.append(a)
        if len(usableAbilities) > 1:
            aType = self.selector.abilitySelection(card, usableAbilities)[0]
        elif len(usableAbilities) == 1:
            aType = usableAbilities[0]
        else:
            aType = None
        return aType

    def addTrigger(self, condition, effect):
        self.triggerConditions.append(condition)
        self.triggerEffects.append(effect)

    def validatePurchase(self, slotNum):
        tradeRow = self.cp.tradeRow
        slot = tradeRow[slotNum - 1]
        # Nullity Check
        if len(slot) and tradeRow[slotNum - 1][0].name != None:
            card = tradeRow[slotNum - 1][0]
        else:
            self.gameWindow.notify(f"There is no card in slot {slotNum}.")
            return False
        cost = CARD_DATA[card.name][C_ENUM["COST"]]
        if cost > self.cp.trade:
            self.gameWindow.notify(f"Insufficient trade to purchase {card.name}.")
            return False
        fee = self.cp.tradeCurve[slotNum - 1]
        if cost + fee > self.cp.trade:
            self.gameWindow.notify(f"Insufficient trade to pay cost of {cost} and fee of {fee}.")
            return False
        return True

    ## @precondition: the purchase must be validated.
    def purchaseCard(self, slotNum, forFree=False, forceDst=False):
        slot = self.cp.tradeRow[slotNum - 1]
        card = slot[0]
        if not forFree:
            self.cp.trade -= CARD_DATA[card.name][C_ENUM["COST"]]
            self.gameWindow.notify(self.cp.name + " purchased " + card.name + ".")
        else:
            self.gameWindow.notify(self.cp.name + " acquired " + card.name + ".")
        self.cp.trade -= self.cp.tradeCurve[slotNum - 1]
        self.gameWindow.updateStats()

        p = card.primary
        if p != () and (p[0] == "blitz" or type(p[1]) is tuple and ("blitz", None) in p[1]) and \
                self.numPlayed(f=card.faction) > 0 and not forceDst:
            dst = "H"
        else:
            dst = self.handler.destinations[card.type]

        if dst == "-":
            drawCards(slot, self.cp.discPile, 1)
            self.gameWindow.updateDiscPile()
        elif dst == "H":
            drawCards(slot, self.cp.hand, 1)
            self.gameWindow.displayHand()
            self.gameWindow.displayField()
        elif dst == "P":
            drawCards(slot, self.temp, 1)
        elif dst == "D":
            drawCards(slot, self.cp.drawPile, 1)
            self.gameWindow.updateDrawLabel()
        elif dst == "S":
            drawCards(slot, self.cp.drawPile, 1)
            shuffle(self.cp.drawPile)
            self.gameWindow.updateDrawLabel()

        if card.type in self.handler.dstLink:
            self.handler.resetDst(self.handler.dstLink)
        self.replaceTradeRowCards([slotNum - 1])
        self.gameWindow.updateTradeRow()

        if dst == "P":
            self.playCard(card)

    ## @precondition: the card must be a base.
    def validateDestroyBase(self, base):
        if base.isO or not self.op.hasOutposts():
            if self.cp.combat >= base.health:
                return True
            else:
                self.gameWindow.notify(f"Insufficient combat to destroy {base.name}.")
        else:
            self.gameWindow.notify("Outposts must be targeted before other bases.")
        return False

    ## @precondition: the attack must be verified.
    def destroyBase(self, base):
        self.gameWindow.notify(base.name + " destroyed.")
        self.cp.combat -= base.health
        moveCards(self.op.field, self.op.discPile, [base])
        self.gameWindow.updateStats()
        self.gameWindow.displayWall()

    ## Replaces cards that were removed from the trade row or Explorer pile.
    def replaceTradeRowCards(self, indices):
        for index in indices:
            slot = self.tradeRow[index]
            if index != TRADE_ROW_LEN:
                if (index < TRADE_ROW_LEN // 2) == (self.cp.pNum == 1):
                    player = self.cp
                else:
                    player = self.op
                if len(player.tDeck):
                    drawCards(player.tDeck, slot, 1)
                else:
                    self.gameWindow.notify(f"{player.name}'s trade deck is empty.")
                    slot.append(Card(None))
            else:
                self.cp.tradeRow[TRADE_ROW_LEN].append(Card("Explorer"))

    ## Determines how many cards were played this turn whose faction is
    ## at least partly in f and primary type is in t. If unaligned cards
    ## should count toward the total, then insert "-" somewhere in f.
    def numPlayed(self, f="GYBR-", t="SBH"):
        return numMatches(self.cardsPlayed, f, t)

    ## Determines how many cards of a certain faction and card type are in play.
    def numInPlay(self, f="GYBR-", t="SBH"):
        currentCards = self.cp.field[:]
        currentCards.extend(self.temp)
        return numMatches(currentCards, f, t)