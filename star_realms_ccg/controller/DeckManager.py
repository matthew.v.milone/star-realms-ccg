## Star Realms CCG
## Version 2.3.1
##
## Contains the back-end logic used for the deck-editing system.
##
## @author: Matthew Milone


from ..view.DeckManagerWindow import DeckManagerWindow
from ..common.constants import CARD_DATA, C_ENUM, DECK_SIZE


class DeckManager(object):
    def __init__(self, debug=False):
        self.debug = debug
        self.deckManagerWindow = None
        self.deckNames = ["Player 1", "Player 2"]

    def makeWindow(self, root, debug):
        self.debug = debug
        self.deckManagerWindow = DeckManagerWindow(root, self)

    def notify(self, message):
        self.deckManagerWindow.notify(message, self.debug)

    def validateDeck(self, deck):
        '''
        Reports the first error found in the deck.
        :param deck: raw text string from a deck file
        :return: blank string if the deck is valid, or an error message if it is invalid.
        '''
        lines = deck.splitlines()
        entries = [line[1:] for line in lines if len(line) and line[0] == "-"]
        names = []
        for entry in entries:
            if entry.count("(") == 1 and entry.count(")") == 1:
                nstr = ""
                for i in range(entry.index("(") + 1, entry.index(")")):
                    nstr += entry[i]
                copies = int(nstr)
                tokens = entry.split(" ")
                name = " ".join(tokens[:-1])
                names.extend(copies * [name])
            else:
                names.append(entry)
        if (l:= len(names)) > DECK_SIZE:
            return f"Decks must have exactly {DECK_SIZE} cards. ({l} found)"
        allCardNames = list(CARD_DATA.keys())
        invalidNames = [name for name in names if name not in allCardNames]
        invalidNamesStr = ' '.join(invalidNames)
        if invalidNames:
            return f"Unrecognized card name(s): {invalidNamesStr}"
        num8s = len([card for card in names if CARD_DATA[card][C_ENUM["COST"]] >= 8])
        num7s = len([card for card in names if CARD_DATA[card][C_ENUM["COST"]] >= 7])
        num6s = len([card for card in names if CARD_DATA[card][C_ENUM["COST"]] >= 6])
        num5s = len([card for card in names if CARD_DATA[card][C_ENUM["COST"]] >= 5])
        if num8s > 3:
            return "Too many cards of cost 8 or above."
        elif num7s > 6:
            return "Too many cards of cost 7 or above."
        elif num6s > 9:
            return "Too many cards of cost 6 or above."
        elif num5s > 12:
            return "Too many cards of cost 5 or above."
        countDict = {}
        for name in names:
            if name in countDict:
                countDict[name] += 1
            else:
                countDict[name] = 1
        for card in countDict.keys():
            if (c := countDict[card]) > (m := CARD_DATA[card][C_ENUM["COPIES"]]):
                return f"Only {m} copies of {card} are allowed. ({c} found)"
        return ""