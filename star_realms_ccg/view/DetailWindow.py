## Star Realms CCG
## Version 2.3.1
##
## Contains the class definition of a detail/portrait window, which shows
## the detailed image of a card. If can also be instructed to show the
## card's available abilities.
##
## If the window is closed by left- or right-clicking.
##
## @author: Matthew Milone

from tkinter import *

from ..common.constants import GUI_IMAGE_DIR
from ..common.constants import ABILITY_TYPES


class DetailWindow(Toplevel):
    def __init__(self, card, withAbilities=False, parentWindow=None):
        Toplevel.__init__(self, parentWindow)
        self.title(card.name)
        self.grab_set()
        bigImage = card.bigImage
        bigBtn = Button(self, image=bigImage, command=self.destroy)
        bigBtn.bind("<Button-3>", self.wrappedDestroy)
        bigBtn.image = bigImage
        bigBtn.pack()
        self.grab_set()
        ## Defines a frame that shows which of the card's abilities are unused.
        if withAbilities:
            abilityFrame = Frame(self)
            abilityFrame.pack()
            abilityLabels = []
            abilityKeys = list(card.unusedAbilities)
            for aType in ABILITY_TYPES:
                if aType in abilityKeys:
                    if aType == "@":
                        symbol = card.faction
                    else:
                        symbol = aType[-1]
                    picture = GUI_IMAGE_DIR + symbol + "_icon.gif"
                    icon = PhotoImage(file=picture).subsample(3)
                    label = Label(self, image=icon)
                    abilityLabels.append(label)
                    label.image = icon
            for label in abilityLabels:
                label.pack(side=LEFT)

    def wrappedDestroy(self, void=None):
        self.grab_release()
        if not self.master:
            self.master.grab_set()
        self.destroy()