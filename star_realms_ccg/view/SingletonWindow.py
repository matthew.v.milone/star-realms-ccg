## Star Realms CCG
## Version 2.3.1
##
## Contains the class definition of a text selection window. A selection
## window is used when the player selects abilities from a card.
##
## If the window is closed before a selection is made, then an empty
## string is returned.
##
## @author: Matthew Milone


from tkinter import Toplevel, Label, Frame, Button, PhotoImage, TOP, BOTTOM, LEFT, RIGHT

from ..common.constants import GUI_IMAGE_DIR
from .SingletonButton import SingletonButton


class SingletonWindow(Toplevel):
    def __init__(self, controller, card, inputs, mini, maxi):
        Toplevel.__init__(self)
        self.title("Star Realms CCG")
        self.inputs = inputs
        self.controller = controller

        if mini == 1 and maxi == 1:
            instructions = "Select an option."
        else:
            instructions = f"Select {mini}-{maxi} options."
        Label(self, text=instructions, font="Ariel 24").pack()
        picLabel = Label(self, image=card.bigImage)
        picLabel.pack()

        buttonFrame = Frame(self)
        buttonFrame.pack()
        if mini != maxi:
            self.doneImage = PhotoImage(file=GUI_IMAGE_DIR+"Done.gif")
            doneBtn = Button(buttonFrame, image=self.doneImage, bd=1, command=self.checkDone)
            doneBtn.pack(side=RIGHT)
        for i in inputs:
            newBtn = SingletonButton(buttonFrame, i, card, self.controller)
            newBtn.pack(side=LEFT)

        self.choices = []

    def checkDone(self):
        self.controller.checkDone()