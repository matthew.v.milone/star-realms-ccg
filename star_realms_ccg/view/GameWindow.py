## Star Realms CCG
## Version 2.3.1
##
## Contains the game's primary GUI window.
##
## @author: Matthew Milone


import sys

import tkinter as tk

from star_realms_ccg.view.DetailWindow import DetailWindow
from star_realms_ccg.view.HandButton import HandButton
from star_realms_ccg.view.StatBlock import StatBlock
from star_realms_ccg.view.TradeRowButton import TradeRowButton
from star_realms_ccg.view.WallButton import WallButton
from star_realms_ccg.view.FieldButton import FieldButton
from star_realms_ccg.common.constants import GUI_IMAGE_DIR, TRADE_ROW_LEN, VERSION

BUFFER = 8
DISC_SHRINK_FACTOR = 1
STAT_SHRINK_FACTOR = 2
HSCROLL_WIDTH = 956


class GameWindow(tk.Toplevel):
    def __init__(self, root, tradeRow, currentPlayer, opposingPlayer, controller, DEBUG):
        tk.Toplevel.__init__(self, root)
        self.title("Star Realms CCG | v" + VERSION)
        self.cp = currentPlayer
        self.op = opposingPlayer
        self.controller = controller
        self.DEBUG = DEBUG

        self.grab_set()
        self.master.wm_withdraw()

        screenWidth = self.winfo_screenwidth()
        screenHeight = self.winfo_screenheight()
        self.wm_geometry("{}x{}+0+0".format(screenWidth, screenHeight))
        self.protocol("WM_DELETE_WINDOW", self.__close__)

        if self.DEBUG:
            self.bind("c", self.__debugCombat__)
            self.bind("t", self.__debugTrade__)
            self.bind("a", self.__debugAuthority__)

        ### Upper Pane ###
        highFrame = tk.Frame(self, height=160)
        highFrame.height=160
        highFrame.pack()

        ## Enemy's Field ##
        opCardsFrame = tk.Frame(highFrame, bd=5, relief=tk.GROOVE)
        opCardsFrame.pack(side=tk.LEFT)
        self.opScrollCanvas = tk.Canvas(opCardsFrame)
        self.opFieldFrame = tk.Frame(self.opScrollCanvas, padx=BUFFER, pady=BUFFER,
                                  bd=4, relief="ridge", bg="#800000", width=798, height=104)
        opScrollbar = tk.Scrollbar(opCardsFrame, orient="horizontal", command=self.opScrollCanvas.xview)
        self.opScrollCanvas.configure(xscrollcommand=opScrollbar.set)
        opScrollbar.pack(side="bottom", fill="x")
        self.opScrollCanvas.pack(side=tk.LEFT)
        self.opScrollCanvas.create_window((0,0), window=self.opFieldFrame, anchor="nw")
        self.opFieldFrame.bind("<Configure>", self.__topScroll__)

        ## Enemy's Stats ##
        opStatsGrid = tk.Frame(highFrame, bd=5, relief=tk.GROOVE)
        opStatsGrid.pack(side=tk.LEFT)
        self.opNameLabel = tk.Label(opStatsGrid, text=self.op.name, font="Helvetica 20")
        self.opAuthority = StatBlock(opStatsGrid, self.op.authority,
                                     GUI_IMAGE_DIR+"authority.gif", STAT_SHRINK_FACTOR)
        self.opNameLabel.grid(row=0,column=0)
        self.opAuthority.grid(row=1,column=0)

        ### Trade Row Pane ###
        trFrame = tk.Frame(self)
        trFrame.pack()
        self.trBorder = tk.Label(trFrame, image=tk.PhotoImage(file=GUI_IMAGE_DIR+"p1_trade_border.gif"))
        self.trBorder.pack(side=tk.TOP)
        
        ## Create Trade Row Buttons ##
        self.trButtons = []
        trButtonFrame = tk.Frame(trFrame)
        trButtonFrame.pack(side=tk.TOP)
        for i in range(0, TRADE_ROW_LEN):
            self.trButtons.append(TradeRowButton(trButtonFrame, tradeRow, i, controller, self))
            self.trButtons[i].pack(side=tk.LEFT)

        ### Text-Based Output Frame ###
        textIOFrame = tk.Frame(self)
        textIOFrame.pack(side=tk.TOP)
        self.outputBox = tk.Label(textIOFrame, text="Welcome to Star Realms.", font="16")
        self.outputBox.pack(side=tk.LEFT)
        #inputBox = Entry(textIOFrame)
        #inputBox.pack(side=RIGHT)

        ### Lower Pane ###
        lowFrame = tk.Frame(self)
        lowFrame.pack(side=tk.TOP)

        ## Two-Button Frame ##
        twoBtnFrame = tk.Frame(lowFrame, width=500)
        twoBtnFrame.pack(side=tk.LEFT)

        # Explorer Button #
        explorerBtn = TradeRowButton(twoBtnFrame, tradeRow, 8, controller, self)
        explorerBtn.pack(side=tk.TOP)
        
        # End-of-Turn Button #
        #endTurnImage = PhotoImage(file="")
        self.endTurnImage = tk.PhotoImage(file=GUI_IMAGE_DIR+"End_Turn.gif")
        endTurnBtn = tk.Button(twoBtnFrame, image=self.endTurnImage, bd=0, command=self.endTurn)
        endTurnBtn.pack()

        ## Player's Cards ##
        cpCardsFrame = tk.Frame(lowFrame, bd=5, relief=tk.GROOVE)
        cpCardsFrame.pack(side=tk.LEFT)
        self.cpScrollCanvas = tk.Canvas(cpCardsFrame)

        # Scrollbar #
        self.cpScrollbar = tk.Scrollbar(cpCardsFrame, orient="horizontal", command=self.cpScrollCanvas.xview)
        self.cpScrollCanvas.configure(xscrollcommand=self.cpScrollbar.set)
        self.cpScrollbar.pack(side="bottom", fill="x")
        self.cpScrollCanvas.pack(side=tk.LEFT)
        
        # Hand Frame #
        self.handFrame = tk.Frame(self.cpScrollCanvas, padx=BUFFER, pady=BUFFER, bd=4, relief="ridge", bg="#000080")
        self.cpScrollCanvas.create_window((0,0), window=self.handFrame, anchor="nw")
        self.handFrame.bind("<Configure>", self.__bottomScroll__)

        # Field Frame #
        self.cpFieldFrame = tk.Frame(self.cpScrollCanvas, padx=BUFFER, pady=BUFFER, bd=4, relief="ridge")

        ## Active Player Panel ##
        cpFrame = tk.Frame(lowFrame)
        cpFrame.pack(side=tk.LEFT)

        # Stats Grid #
        statsGrid = tk.Frame(cpFrame, width=100, bd=5, relief=tk.GROOVE)
        statsGrid.pack(side=tk.LEFT)
        self.cpNameLabel = tk.Label(statsGrid, text=self.cp.name, font="Helvetica 20")
        self.cpAuthority = StatBlock(statsGrid, self.cp.authority, GUI_IMAGE_DIR+"authority.gif", STAT_SHRINK_FACTOR)
        self.cpTrade = StatBlock(statsGrid, self.cp.trade, GUI_IMAGE_DIR+"trade.gif", STAT_SHRINK_FACTOR)
        self.cpCombat = StatBlock(statsGrid, self.cp.combat, GUI_IMAGE_DIR+"combat.gif", STAT_SHRINK_FACTOR)
        self.cpNameLabel.grid(row=0,column=0)
        self.cpAuthority.grid(row=1,column=0)
        self.cpTrade.grid(row=2,column=0)
        self.cpCombat.grid(row=3,column=0)

        # Lower-Right Pane #
        self.lowerRightPane = tk.Frame(cpFrame)
        self.lowerRightPane.pack(side=tk.LEFT)

        # Discard Pile 
        self.emptyDiscPileImage = tk.PhotoImage(file=GUI_IMAGE_DIR+"discard_pile.gif").subsample(2*DISC_SHRINK_FACTOR)
        self.discPileBtn = tk.Button(self.lowerRightPane, image=self.emptyDiscPileImage,
                                  command=lambda: self.discPileDetail(None))
        self.discPileBtn.pack(side=tk.TOP)

        # Draw Pile Label 
        self.drawPileLabel = tk.Label(self.lowerRightPane,
                                      text=f"{len(self.cp.discPile)} Remaining", font="Helvetica 18")
        self.drawPileLabel.pack(side=tk.TOP)

    def enterLoop(self):
        self.mainloop()

    def exitLoop(self):
        self.quit()

    def notify(self, message):
        self.outputBox.configure(text=message)
        self.outputBox.text = message
        if self.DEBUG:
            print(message)

    def swapPlayers(self):
        self.cp, self.op = self.op, self.cp

    def update(self):
        self.updateStats()
        self.updateOPStats()
        self.updateTradeRow()
        self.updateTRBorder()
        self.updateDiscPile()
        self.updateDrawLabel()
        self.displayHand()
        self.displayField()
        self.displayWall()

    #Updates the stats on the low bar to reflect those of the given player.
    def updateStats(self):
        self.cpNameLabel.configure(text=self.cp.name)
        self.cpAuthority.configure(text=self.cp.authority)
        self.cpTrade.configure(text=self.cp.trade)
        self.cpCombat.configure(text=self.cp.combat)

    def updateOPStats(self):
        self.opNameLabel.configure(text=self.op.name)
        self.opAuthority.configure(text=self.op.authority)

    def updateTradeRow(self, indices=[x for x in range(8)]):
        for x in indices:
            self.trButtons[x].updateImage()

    def updateTRBorder(self):
        self.trBorder.configure(image=self.cp.trBorder)

    def updateDiscPile(self):
        if len(self.cp.discPile) > 0:
            newImage = self.cp.discPile[-1].smallImage.subsample(DISC_SHRINK_FACTOR)
            self.discPileBtn.configure(image=newImage)
            self.discPileBtn.image = newImage
        else:
            self.discPileBtn.configure(image=self.emptyDiscPileImage)

    def updateDrawLabel(self):
        self.drawPileLabel.configure(text=f"{len(self.cp.drawPile)} in Draw Pile")

    def displayHand(self):
        for button in self.handFrame.winfo_children():
            button.destroy()
        l = len(self.cp.hand)
        for n in range(l):
            card = self.cp.hand[n]
            btn = HandButton(self.handFrame, card, self.controller, self)
            btn.grid(row=0, column=n)

    def displayField(self):
        handWidth = 158*len(self.cp.hand)
        self.cpFieldFrame.destroy()
        self.cpFieldFrame = tk.Frame(self.cpScrollCanvas, padx=BUFFER, pady=BUFFER,
                                  bd=4, relief="ridge", bg="#800000")
        self.cpScrollCanvas.create_window((handWidth + 3 * BUFFER, 0), window=self.cpFieldFrame, anchor="nw")
        self.cpFieldFrame.bind("<Configure>", self.__bottomScroll__)
        for n in range( (l := len(self.cp.field)) ):
            card = self.cp.field[n]
            btn = FieldButton(self.cpFieldFrame, self.controller, self, card)
            btn.image = card.smallImage
            btn.grid(row=0, column=l-n)

    def displayWall(self):
        for button in self.opFieldFrame.winfo_children():
            button.destroy()
        l = len(self.op.field)
        for n in range(l):
            card = self.op.field[n]
            btn = WallButton(self.opFieldFrame, card, self.controller, self)
            btn.grid(row=0, column=n)

    def discPileDetail(self, void):
        if len(self.cp.discPile):
            self.showDetail(self.cp.discPile[-1])

    def showDetail(self, card, withAbilities=False, middleWindow=None):
        if middleWindow != None:
            middleWindow.grab_release()
        DetailWindow(card, withAbilities, middleWindow)

    def endTurn(self):
        self.controller.endTurn()

    def endGame(self):
        self.cp, self.op = self.op, self.cp
        self.updateStats()
        self.updateOPStats()
        if self.op.authority <= 0:
            self.notify(f"Congratulations, {self.cp.name}. You won!")
        elif self.op.authority > 100:
            self.notify(f"Game drawn by authority accumulation.")
        exitGameWindow = tk.Toplevel()
        exitGameWindow.grab_set()
        self.exitGameImage = tk.PhotoImage(file=GUI_IMAGE_DIR+"Exit_Game.gif")
        exitGameBtn = tk.Button(exitGameWindow, image=self.exitGameImage, bd=0,
                                command=lambda: self.__close__(exitGameWindow))
        exitGameBtn.pack()
        self.enterLoop()

    def __close__(self, window=None):
        if window: window.destroy()
        self.master.wm_deiconify()
        self.destroy()

    def __debugCombat__(self, event):
        self.cp.combat += 10
        self.updateStats()

    def __debugTrade__(self, event):
        self.cp.trade += 100
        self.updateStats()

    def __debugAuthority__(self, event):
        self.cp.authority += 20
        self.updateStats()

    def __topScroll__(self, event):
        self.opScrollCanvas.configure(scrollregion=self.opScrollCanvas.bbox("all"), width=798, height=104)

    def __bottomScroll__(self, event):
        self.cpScrollCanvas.configure(scrollregion=self.cpScrollCanvas.bbox("all"), width=798, height=226+2*BUFFER)