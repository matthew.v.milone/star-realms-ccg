## Star Realms CCG
## Version 2.3.1
##
## Buttons in the deck manager corresponding to decks.
##
## @author: Matthew Milone


import tkinter as tk


class DeckBar(tk.Button):
    def __init__(self, root, parentWindow, deckName, i):
        tk.Button.__init__(self, root, text=deckName, font="Helvetica 18", command=self.editDeck)
        self.parentWindow = parentWindow
        self.i = i


    def editDeck(self):
        self.parentWindow.editDeck(self.i)