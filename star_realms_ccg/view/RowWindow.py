## Star Realms CCG
## Version 2.3.1
##
## Contains the class definition of a row selection window. A row selection
## window is used when the player is forced to select cards from a set of lists.
##
## @author: Matthew Milone


import tkinter as tk

from ..common.constants import GUI_IMAGE_DIR
from star_realms_ccg.view.GameWindow import HSCROLL_WIDTH
from .HScrollFrame import HScrollFrame


class RowWindow(tk.Toplevel):
    def __init__(self, container, selector, namedCardLists, instructions, mini, maxi):
        tk.Toplevel.__init__(self, container)
        self.title("Star Realms CCG")

        self.selector = selector
        
        ioFrame = tk.Frame(self, height=58)
        ioFrame.pack(fill="x")
        ioFrame.pack_propagate(0)
        if mini != maxi:
            self.doneImage = tk.PhotoImage(file=GUI_IMAGE_DIR+"Done.gif")
            doneBtn = tk.Button(ioFrame, image=self.doneImage, command=self.checkDone)
            doneBtn.place(anchor=tk.NW, x=2, y=2)
        iLabel = tk.Label(ioFrame, text=instructions, font="Helvetica 20 underline")
        iLabel.place(relx=.5, rely=.5, anchor=tk.CENTER)

        mainFrame = tk.Frame(self)
        mainFrame.pack()
        self.canvas = tk.Canvas(mainFrame)
        innerFrame = tk.Frame(self.canvas)
        scrollbar = tk.Scrollbar(mainFrame, orient="vertical", command=self.canvas.yview)
        self.canvas.configure(yscrollcommand=scrollbar.set)
        scrollbar.pack(side=tk.RIGHT, fill="y")
        self.canvas.pack()
        self.canvas.create_window((0,0), window=innerFrame, anchor="nw")
        innerFrame.bind("<Configure>", self.scroll)

        for item in namedCardLists.items():
            if item[1]:
                name, cardList = item[0], item[1]
                tk.Label(innerFrame, text=name, font="Helvetica 16").pack()
                HScrollFrame(innerFrame, name, cardList, selector, self).pack()

    def showDetail(self, card):
        self.selector.showDetail(card, self)

    def checkDone(self):
        self.selector.checkDone()

    def scroll(self, void):
        self.canvas.configure(scrollregion=self.canvas.bbox("all"),width=HSCROLL_WIDTH,height=300)