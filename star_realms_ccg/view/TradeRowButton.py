## Star Realms CCG
##
## Version 2.3.1
## Contains the class file for the buttons in the trade row.
##
## @author: Matthew Milone


from tkinter import Button, RIDGE


class TradeRowButton(Button):
    def __init__(self, container, tradeRow, index, controller, gameWindow):
        self.gameWindow = gameWindow
        self.controller = controller
        self.slotNum = index + 1
        self.slot = tradeRow[index]
        Button.__init__(self, container, image=self.slot[0].smallImage, bd=2,
                        relief=RIDGE, background="#000000", command=self.buyCard)
        self.bind("<Button-3>", self.showDetail)

    ## Updates the button's image.
    def updateImage(self):
        newImage = self.slot[0].smallImage
        self.configure(image=newImage)
        self.image = newImage

    ## Creates a new window with a detailed image of a card on it.
    ## The window disappears on-click.
    def showDetail(self, void):
        self.gameWindow.showDetail(self.slot[0])

    ## Buys a card from the trade row iff the purchase is legal.
    def buyCard(self):
        if self.controller.validatePurchase(self.slotNum):
            self.controller.purchaseCard(self.slotNum)
            self.updateImage()