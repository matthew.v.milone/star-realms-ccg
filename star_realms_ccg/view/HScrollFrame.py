## Star Realms CCG
## Version 2.3.1
##
## Contains the class definition for a horizontally scrollable TK frame.
##
## @author: Matthew Milone


from tkinter import *

from ..view.SelectionButton import SelectionButton
from .GameWindow import HSCROLL_WIDTH


class HScrollFrame(Frame):
    def __init__(self, container, listName, cardList, controller, parentWindow=None):
        Frame.__init__(self, container)

        outerFrame = Frame(self)
        outerFrame.pack(fill="x")
        self.canvas = Canvas(outerFrame)
        innerFrame = Frame(self.canvas)
        scrollbar = Scrollbar(outerFrame, orient="horizontal", command=self.canvas.xview)
        self.canvas.configure(xscrollcommand=scrollbar.set)
        scrollbar.pack(side=BOTTOM, fill="x")
        self.canvas.pack()
        self.canvas.create_window((0, 0), window=innerFrame, anchor="nw")
        innerFrame.bind("<Configure>", self.scroll)

        for i in range(len(cardList)):
            card = cardList[i]
            SelectionButton(innerFrame, card, listName, i, controller, parentWindow).pack(side=LEFT)

    def scroll(self, void):
        self.canvas.configure(scrollregion=self.canvas.bbox("all"), width=HSCROLL_WIDTH, height=220)
