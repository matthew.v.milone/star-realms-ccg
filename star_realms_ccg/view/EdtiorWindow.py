## Star Realms CCG
## Version 2.3.1
##
## Contains the window used to edit a deck.
##
## @author: Matthew Milone


import os

import tkinter as tk
from tkinter import filedialog

from ..common.constants import P1_FILE, P2_FILE


class EditorWindow(tk.Toplevel):
    def __init__(self, root, controller, deckName):
        tk.Toplevel.__init__(self, root)
        self.controller = controller
        self.deckName = deckName
        self.title(deckName)
        #self.wm_geometry("800x800")

        self.protocol("WM_DELETE_WINDOW", self.__close__)
        self.grab_set()

        self.textWidget = tk.Text(self)
        self.textWidget.grid()
        text = open(os.getcwd() + "/data/" + deckName + ".txt").read()
        self.textWidget.insert(tk.CURRENT, text)

    def __close__(self):
        text = self.textWidget.get(1.0, tk.END)
        message = self.controller.validateDeck(text)
        self.controller.notify(message)
        open(os.getcwd() + "/data/" + self.deckName + ".txt", "w").write(text[:-1])
        self.master.grab_set()
        self.destroy()