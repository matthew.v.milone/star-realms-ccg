## Star Realms CCG
## Version 2.3.1
##
## Contains the class file for the buttons in the active player's hand.
##
## @author: Matthew Milone


from tkinter import *

from ..common.helper import isAllStats


class HandButton(Button):
    def __init__(self, container, card, controller, gameWindow):
        self.gameWindow = gameWindow
        self.controller = controller
        self.card = card
        Button.__init__(self, container, image=card.smallImage, bd=1, command=self.playCard)
        self.bind("<Button-3>", self.showDetail)
        self.bind("<Button-2>", self.useBase)

    ## Creates a new window with a detailed image of a card on it.
    ## The window disappears on-click.
    def showDetail(self, void):
        self.gameWindow.showDetail(self.card, withAbilities=True)

    def playCard(self):
        self.gameWindow.exitLoop()
        self.controller.playCard(self.card)

    def useBase(self, void):
        self.playCard()
        if self.card.isBase() and self.card.primary and not isAllStats(self.card.primary):
            self.controller.useAbility(self.card, "P")