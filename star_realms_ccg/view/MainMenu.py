## Star Realms CCG
## Version 2.3.1
##
## Main menu window. Allows user to play game, edit deck, or exit.
##
## @author: Matthew Milone

import sys
import os

import tkinter as tk

from ..common.constants import VERSION, GUI_IMAGE_DIR


class MainMenu(tk.Tk):
    def __init__(self, gameController, deckManager):
        tk.Tk.__init__(self)
        self.title("Star Realms CCG | v" + VERSION)
        self.wm_geometry("400x400")

        self.backgroundImage = tk.PhotoImage(file=GUI_IMAGE_DIR + "main_menu_background.gif")
        self.backgroundLabel = tk.Label(self, image=self.backgroundImage)
        self.backgroundLabel.place(x=0, y=0, anchor=tk.NW)

        self.gameController, self.deckManager = gameController, deckManager

        titleLabel = tk.Label(self, text="Welcome to\nStar Realms CCG", font="Helvetica 18", relief=tk.RIDGE, bd=4)
        titleLabel.place(relx=.5, rely=.2, anchor=tk.CENTER)

        btnFrame = tk.Frame(self)
        btnFrame.place(relx=.5, rely=.5, anchor=tk.N)
        startBtn = tk.Button(btnFrame, text="Play Game", font="Helvetica 18", command=self.startGame)
        startBtn.grid(row=0, column=0, sticky=tk.W+tk.E)
        manageBtn = tk.Button(btnFrame, text="Manage Decks", font="Helvetica 18", command=self.manageDecks)
        manageBtn.grid(row=1, column=0, sticky=tk.W+tk.E)
        exitBtn = tk.Button(btnFrame, text="Quit to Desktop", font="Helvetica 18", command=self.exitApp)
        exitBtn.grid(row=2, column=0, sticky=tk.W+tk.E)

        self.debug = tk.IntVar()
        debugBtn = tk.Checkbutton(self, text="Debug Mode", font="Helvetica 8", variable=self.debug)
        debugBtn.place(relx=1, rely=0, anchor=tk.NE)

        self.outputBox = tk.Label(self, text="", font="Helvetica 12")
        self.outputBox.pack(side=tk.BOTTOM)
        self.outputBox.lower(self.backgroundLabel)

        self.protocol("WM_DELETE_WINDOW", self.exitApp)
        self.mainloop()

    def checkDebug(self):
        return self.debug.get()

    def callback(self):
        self.deckManager.debug = self.checkDebug()

    def notify(self, message, debug):
        if message != "":
            self.outputBox.configure(text=message)
            self.outputBox.lift(self.backgroundLabel)
            if not debug:
                print(message)
        elif self.outputBox:
            self.outputBox.lower(self.backgroundLabel)

    def startGame(self):
        if len(self.deckManager.deckNames) >= 2:
            invalidDeckName = ""
            for deckName in self.deckManager.deckNames:
                text = text = open(os.getcwd() + "/data/" + deckName + ".txt").read()
                deckError = self.deckManager.validateDeck(text)
                if deckError:
                    invalidDeckName = deckName
                    break
            if deckError and not self.checkDebug():
                self.notify(f"Error in deck \"{invalidDeckName}\":\n{deckError}", self.checkDebug())
            else:
                self.notify("", self.checkDebug())
                self.gameController.playGame(self, self.checkDebug())
        else:
            self.notify("Error:\nthere must be at least two decks.")

    def manageDecks(self):
        self.notify("", self.checkDebug())
        self.deckManager.makeWindow(self, self.checkDebug())

    def exitApp(self):
        self.destroy()
        sys.exit()