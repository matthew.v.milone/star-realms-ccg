## Star Realms CCG
## Version 2.3.1
##
## Contains the class for the buttons used to interact with
## the opponent's bases.
##
## @author: Matthew Milone


from tkinter import Button


class WallButton(Button):
    def __init__(self, container, card, controller, gameWindow):
        self.card = card
        self.controller = controller
        self.gameWindow = gameWindow
        picture = card.bigImage.subsample(4)
        Button.__init__(self, container, image=picture, bd=0, command=self.destroyBase)
        self.image=picture
        self.bind("<Button-3>", self.showDetail)

    def showDetail(self, void):
        self.gameWindow.showDetail(self.card)

    def destroyBase(self):
        if self.card.isBase():
            if self.controller.validateDestroyBase(self.card):
                self.controller.destroyBase(self.card)
        else:
            self.gameWindow.notify(f"{self.card.name} is not a base.")