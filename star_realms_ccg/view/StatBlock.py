## Star Realms CCG
## Version 2.3.1
##
## Contains a label widget representing a quantity important to the game.
##
## @author: Matthew Milone


from tkinter import Label, PhotoImage


class StatBlock(Label):
    def __init__(self, container, quantity, picture, sFactor):
        icon = PhotoImage(file=picture).subsample(sFactor)
        fontSize = 16
        super(StatBlock, self).__init__(container, text=quantity, image=icon, compound="center", font=str(fontSize))
        self.image = icon

    def update(self, newNum):
        self.configure(text=newNum)
        self.text = newNum