## Star Realms CCG
## Version 2.3.1
##
## Contains the class file for the buttons in the active player's field.
##
## @author: Matthew Milone


from tkinter import Button


class FieldButton(Button):
    def __init__(self, container, controller, gameWindow, card):
        self.controller = controller
        self.gameWindow = gameWindow
        self.card = card
        Button.__init__(self, container, image=card.smallImage, bd=1, command=self.useAbility)
        self.bind("<Button-3>", self.showDetail)

    ## Creates a new window with a detailed image of a card on it.
    ## The window disappears on-click.
    def showDetail(self, void):
        self.gameWindow.showDetail(self.card, withAbilities=True)

    ## Chooses one of the card's abilies to use.
    def useAbility(self):
        if len(self.card.unusedAbilities):
            abilityType = self.controller.chooseAbility(self.card)
            if abilityType != None:
                self.controller.useAbility(self.card, abilityType)
            else:
                self.gameWindow.notify(f"{self.card.name} has no usable abilities.")
        else:
            self.gameWindow.notify(f"{self.card.name} has no unused abilities.")