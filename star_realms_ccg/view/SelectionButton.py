## Star Realms CCG
## Version 2.3.1
##
## Contains the class definion for the buttons used in a row selection window.
##
## @author: Matthew Milone

from tkinter import *


class SelectionButton(Button):
    def __init__(self, container, card, listName, index, controller, parentWindow=None):
        self.parentWindow = parentWindow
        self.listName = listName
        self.index = index
        self.controller = controller
        self.card = card
        picture=card.smallImage
        Button.__init__(self, container, image=picture, bd=2, command=self.selectCard)
        self.image=picture
        self.bind("<Button-3>", self.showDetail)
        self.isSelected = False

    def showDetail(self, void):
        self.parentWindow.showDetail(self.card)

    def selectCard(self):
        if self.isSelected:
            self.isSelected = False
            self.configure(relief=RAISED)
            self.controller.deselectCard(self.listName, self.index)
        else:
            self.isSelected = True
            self.configure(relief=SUNKEN)
            self.controller.selectCard(self.listName, self.index)