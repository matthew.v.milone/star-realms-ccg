## Star Realms CCG
## Version 2.3.1
##
## Contains the window used to select a deck to edit.
##
## @author: Matthew Milone


import os

import tkinter as tk

from .EdtiorWindow import EditorWindow
from .DeckBar import DeckBar
from ..common.constants import VERSION, GUI_IMAGE_DIR


class DeckManagerWindow(tk.Toplevel):
    def __init__(self, root, controller):
        tk.Toplevel.__init__(self, root)
        self.controller = controller
        self.title("Star Realms CCG | v" + VERSION)
        self.wm_geometry("400x400")

        self.backgroundImage = tk.PhotoImage(file=GUI_IMAGE_DIR + "main_menu_background.gif")
        self.backgroundLabel = tk.Label(self, image=self.backgroundImage)
        self.backgroundLabel.place(x=0, y=0, anchor=tk.NW)

        self.grab_set()
        self.master.wm_withdraw()

        topLabel = tk.Label(self, text="Choose a Deck", font="Helvetica 18", relief=tk.RIDGE, bd=4)
        topLabel.place(relx=.5, rely=.2, anchor=tk.CENTER)

        deckGridFrame = tk.Frame(self)
        deckGridFrame.place(relx=.5, rely=.5, anchor=tk.N)
        for deckName, i in zip(self.controller.deckNames, range(len(self.controller.deckNames))):
            deckBar = DeckBar(deckGridFrame, self, deckName, i)
            deckBar.grid(row=i)

        backBtn = tk.Button(self, text="Back", font="Helvetica 12", command=self.__close__)
        backBtn.place(x=0, y=0)

        self.outputBox = tk.Label(self, text="", font="Helvetica 12")
        self.outputBox.pack(side=tk.BOTTOM)
        self.outputBox.lower(self.backgroundLabel)

        self.protocol("WM_DELETE_WINDOW", self.__close__)

    def editDeck(self, i):
        fileName = self.controller.deckNames[i]
        EditorWindow(self, self.controller, fileName)

    def notify(self, message, debug):
        if message != "":
            self.outputBox.configure(text="Warning:\n"+message)
            self.outputBox.lift(self.backgroundLabel)
            if not debug:
                print(message)
        elif self.outputBox:
            self.outputBox.lower(self.backgroundLabel)

    def __close__(self):
        self.master.wm_deiconify()
        self.destroy()