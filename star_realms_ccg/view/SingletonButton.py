## Star Realms CCG
## Version 2.3.1
##
## Contains the class definition for buttons used singleton windows.
##
## @author: Matthew Milone


from tkinter import *

from ..common.constants import GUI_IMAGE_DIR


class SingletonButton(Button):
    def __init__(self, container, symbol, card, controller):
        self.symbol = symbol
        self.controller = controller
        if symbol in ["P","@G","@Y","@B","@R","$","1","2","G","Y","B","R"]:
            picture = GUI_IMAGE_DIR + symbol[-1] + "_icon.gif"
            icon = PhotoImage(file=picture).subsample(3)
            Button.__init__(self, container, image=icon, command=self.select)
            self.image = icon
        elif symbol == "@":
            picture = GUI_IMAGE_DIR + card.faction + "_icon.gif"
            icon = PhotoImage(file=picture).subsample(3)
            Button.__init__(self, container, image=icon, command=self.select)
            self.image = icon
        elif "+" in symbol:
            if symbol == "+C":
                picture = GUI_IMAGE_DIR + "combat.gif"
            elif symbol == "+T":
                picture = GUI_IMAGE_DIR + "trade.gif"
            elif symbol == "+A":
                picture = GUI_IMAGE_DIR + "authority.gif"
            icon = PhotoImage(file=picture).subsample(3)
            Button.__init__(self, container, image=icon, command=self.select)
            self.image = icon
        else:
            Button.__init__(self, container, text=symbol, font="Helvetica 18",\
                            command=self.select)
        self.isSelected = False

    def select(self):
        if self.isSelected:
            self.isSelected = False
            self.configure(relief=RAISED)
            self.controller.deselectAbility(self.symbol)
        else:
            self.isSelected = True
            self.configure(relief=SUNKEN)
            self.controller.selectAbility(self.symbol)