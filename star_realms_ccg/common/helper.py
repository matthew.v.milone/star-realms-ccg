## Star Realms CCG
## Version 2.3.1
##
## This file contains helpful functions used by other files.
##
## author: Matthew Milone


import sys
from random import shuffle

from ..model.Base import Base
from ..model.Card import Card
from ..common.constants import CARD_DATA, DECK_SIZE, C_ENUM


## Reads a txt file containing a player's information.
def readPlayerData(fileName, pNum, INITIAL_AUTHORITY, DEBUG):
    MAX_NAME_LENGTH = 7

    playerName = "Player " + str(pNum)
    authority = INITIAL_AUTHORITY
    tradeDeck = []
    discardPile = []
    drawPile = []
    field = []
    hand = []
    
    file = open(fileName, "r")
    costHistogram = [0 for x in range(0, 10)]
    currentList = tradeDeck
    for line in file:
        if "Name:" in line:
            playerName = line[5:].strip()[:MAX_NAME_LENGTH]
        elif "Trade Deck:" in line:
            currentList = tradeDeck
        elif line[0] == "-":
            if line.count("(") == 1 and line.count(")") == 1:
                nStr = ""
                for i in range(line.index("(") + 1, line.index(")")):
                    nStr += line[i]
                copies = int(nStr)
                tokens = line[1:].split()
                name = " ".join(tokens[:-1])
            else:
                copies = 1
                name = line[1:]
                if line.count("\n") == 1:
                    name = name[:-1]
            if name in CARD_DATA:
                canAdd = True
                if currentList == tradeDeck:
                    if not DEBUG:
                        limit = int(CARD_DATA[name][3])
                        cost = int(CARD_DATA[name][0])
                        costHistogram[cost] += copies
                        for x in range(5, 10):
                            n = sum(costHistogram[x:])
                            l = max(3, 3 * (9 - x))
                            if n > l:
                                print(f"Warning: only {l} cards of cost {x}", f"or above are allowed. {name} omitted.")
                                canAdd = False
                        oldCopies = len([n for n in tradeDeck if n == name])
                        totalCopies = copies + oldCopies
                        if totalCopies > limit:
                            print(f"Warning: only {limit} copies of {name} are allowed in a deck--"
                                  f"{copies - limit - oldCopies} copies omitted.")
                            copies = limit - oldCopies
                    if canAdd:
                        for n in range(copies):
                            currentList.append(name)
                    else:
                        costHistogram[cost] -= copies
                else:
                    for n in range(copies):
                        currentList.append(name)
            else:
                print(f"Warning: \"{name}\" ({fileName}) is not a recognized card name. Entry ignored.")
        elif DEBUG:
            if "Authority:" in line:
                authority = int(line[10:])
            elif "Discard Pile:" in line:
                currentList = discardPile
            elif "Draw Pile:" in line:
                currentList = drawPile
            elif "Field:" in line:
                currentList = field
            elif "Hand:" in line:
                currentList = hand
    # Warn player if deck size is non-standard.
    if len(tradeDeck) != DECK_SIZE:   
        print(f"Warning: {fileName} specifies a non-standard deck size. ({len(tradeDeck)}/{DECK_SIZE})")
        if not DEBUG:
            sys.exit()
    # Create the default draw pile.
    if not DEBUG:
        for i in range(2):
            drawPile.append("Viper")
        for i in range(8):
            drawPile.append("Scout")
    file.close()
    return (playerName, authority, tradeDeck, discardPile, drawPile, field, hand)


## Reads a line, adding the appropriate number of copies of the card name
## to lst. If lst is the trade deck, then it will check for and correct
## breaches of the deck-building rules.
def addCopies(line, lst, isTradeDeck):
    if line.count("(") == 1 and line.count(")") == 1:
        nStr = ""
        for i in range(line.index("(") + 1, line.index(")")):
            nStr += line[i]
            copies = int(nStr)
            tokens = line.split()
            name = " ".join(tokens[:-1])
    else:
        copies = 1
        name = line
        if line.count("\n") == 1:
            name = name[:-1]


## Takes a list of card names and returns a list of Cards of the same name.
## @precondition: each cardName must be valid.
def cardsFromNames(cardNames):
    cardsList = []
    for cardName in cardNames:
        if CARD_DATA[cardName][C_ENUM["TYPE"]][0] == "B":
            cardsList.append(Base(cardName))
        else:
            cardsList.append(Card(cardName))
    return cardsList


#Prints a list of cards in a veritcal format, along with their frequency
def vPrint(name, cards):
    if not len(cards):
        print("\n" + name, "is empty.")
    else:
        print("\n" + name + ":")
        n_cards = len(cards)
        unique_cards = []
        copies = []
        for card in cards:
            if unique_cards.count(card.name) == 0:
                unique_cards.append(card.name)
                copies.append(1)
            else:
                copies[unique_cards.index(card.name)] += 1
        for card, n in zip(unique_cards, copies):
            print(format(card, "24s") + str(n) + "/" + str(n_cards))
        print()


## Transfers num cards from star_realms_ccg into dst, drawing from specified backup
## pile if necessary. Causes error if backup pile is needed but unspecified.
## If backup is depleted, an error is generated if allow_runout is False,
## or all available cards are drawn if it is True.
## Returns True if num cards were moved, and False otherwise.
def drawCards(src, dst, num, src_name="star_realms_ccg", backup=["E1"], backup_name="bk", allow_runout=False):
    x = len(src)
    if num <= x:
        for n in range(num):
            dst.append(src.pop())
    elif(backup == ["E1"]):
        print(f"\nError: ran out of cards while drawing from {src_name},"
              f"and no backup pile was specified to be shuffled.")
        sys.exit()
    else:
        for n in range(x):
            dst.append(src.pop())
        y = len(backup)
        rem = num - x
        if rem <= y:
            shuffle(backup)
            drawCards(backup, dst, rem)
            #Move remaining backup cards to star_realms_ccg.
            src[:] = backup[:]
            backup[:] = []
        else:
            if allow_runout:
                drawCards(backup, dst, y)
                return False
            else:
                print("\nError: backup pile ran out of cards.")
                sys.exit()
    return True


def moveCards(src, dst, cards):
    for card in cards:
        src.remove(card)
        dst.append(card)


## Determines whether a card's ally ability can be used.
## @precondition: aType should be the symbol of an ally ability, not P or $
def hasAllyAbility(card, player, aType, factionBonuses):
    numAllies = 0
    activeCards = player.field[:]
    activeCards.remove(card)
    if aType == "@" or aType == "@@":
        abilityFaction = card.faction
    else:
        abilityFaction = aType[1]
    #Check for "proc" effects
    for f in abilityFaction:
        if f in factionBonuses:
            return True
    #Check for traditional allies
    for c in activeCards:
        for x in c.faction:
            if x in abilityFaction:
                numAllies += 1
    if aType == "@@" and numAllies >= 2:
        return True
    elif numAllies >= 1:
        return True
    return False


## Determines whether an ability tuple consists of purely
## stat-gaining abilities.
def isAllStats(ability):
    if ability == ():
        return False
    aName = ability[0]
    aArgs = ability[1]
    if aName in ["+C", "+T", "+A"]:
        return True
    elif aName == "and":
        abilitySublist = [isAllStats(a) for a in aArgs]
        if False in abilitySublist:
            return False
        else:
            return True
    else:
        return False


## Determines how many cards in l whose faction is in f and type is in t.
## If unaligned cards should count toward the total, include "-" in f.
def numMatches(l, f="GYBR-", t="SBH"):
    n = 0
    for card in l:
        if card.type[0] not in t:
            continue
        if len(card.faction) == 0:
            if "-" in f:
                n +=1
        else:
            for c in card.faction:
                if c in f:
                    n += 1
                    break
    return n