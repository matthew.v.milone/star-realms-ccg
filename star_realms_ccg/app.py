## Star Realms CCG
## Version 2.3.1
##
## Creates the main menu window. The app terminates if the menu is closed.
##
## @author: Matthew Milone


from tkinter import Tk

from .controller.DeckManager import DeckManager
from .controller.GameController import GameController
from .view.MainMenu import MainMenu


def run():
    deckManager = DeckManager()
    gameController = GameController()
    menu = MainMenu(gameController, deckManager)