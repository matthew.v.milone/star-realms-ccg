## Star Realms CCG
##
## Version 2.3.1
## Class file for base cards.
##
## @author: Matthew Milone


from ..model.Card import *

class Base(Card):
    def __init__(self, cardName):
        Card.__init__(self, cardName)
        typeString = CARD_DATA[cardName][C_ENUM["TYPE"]]
        self.health = int(typeString[1])
        self.isO = "*" in typeString
        dirName = os.getcwd() + "/"
        fileName = cardName.replace(" ", "_") + ".gif"
        if os.path.isfile(dirName + BASE_IMAGE_DIR + fileName):
            self.smallImage = PhotoImage(file=BASE_IMAGE_DIR + fileName).subsample(2)
        else:
            print(dirName + BASE_IMAGE_DIR + fileName + " does not exist.")
            self.smallImage = PhotoImage(file=BASE_IMAGE_DIR + \
                    "Star_Realms_Sleeve.gif").subsample(2)