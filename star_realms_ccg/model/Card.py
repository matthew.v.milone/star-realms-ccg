## Star Realms CCG
## 
## Version 2.3.1
## Card class file.
##
## @author: Matthew Milone


import os
from copy import deepcopy
from tkinter import PhotoImage

from ..common.constants import CARD_DATA, C_ENUM, IMAGE_DIR, BASE_IMAGE_DIR


class Card(object):
    def __init__(self, cardName):
        self.name = cardName
        if cardName != None:
            card = CARD_DATA[cardName]
            self.cost = card[C_ENUM["COST"]]
            self.faction = card[C_ENUM["FACTION"]]
            self.type = card[C_ENUM["TYPE"]][0]
            dirName = os.getcwd() + "/"
            fileName = cardName.replace(" ", "_") + ".gif"
            if os.path.isfile(dirName + IMAGE_DIR + fileName):
                self.bigImage = PhotoImage(file=IMAGE_DIR+fileName)
            else:
                print(dirName + IMAGE_DIR + fileName + " does not exist.")
                self.bigImage = PhotoImage(file=IMAGE_DIR+"Star_Realms_Sleeve.gif")
            self.smallImage = self.bigImage.subsample(2)
            self.primary = card[C_ENUM["PRIMARY"]]
            self.ally = card[C_ENUM["ALLY"]]
            self.scrap = card[C_ENUM["SCRAP"]]

            self.abilityDict = deepcopy(self.ally)
            if self.primary != ():
                self.abilityDict["P"] = self.primary
            if self.scrap != ():
                self.abilityDict["$"] = self.scrap
            self.unusedAbilities = deepcopy(self.abilityDict)
            self.trueName = self.name
            self.trueFaction = self.faction
            
        else:
            self.bigImage = PhotoImage(file=IMAGE_DIR+"Star_Realms_Sleeve.gif")
            self.smallImage = self.bigImage.subsample(2)

    def isBase(self):
        return self.type[0] == "B"

    ## ability types: P, @, @@, @G, @Y, @B, @R, $
    def markAbility(self, ability):
        del self.unusedAbilities[ability]

    def restoreAbilities(self):
        self.unusedAbilities = deepcopy(self.abilityDict)

    def imitate(self, cardName, extraFaction=""):
        card = CARD_DATA[cardName]
        self.name = cardName
        self.cost = card[C_ENUM["COST"]]
        self.faction = card[C_ENUM["FACTION"]]
        if extraFaction not in self.faction:
            self.faction = card[C_ENUM["FACTION"]] + extraFaction
        self.type = card[C_ENUM["TYPE"]]
        dirName = os.getcwd() + "/"
        fileName = cardName.replace(" ", "_") + ".gif"
        if os.path.isfile(dirName + IMAGE_DIR + fileName):
            self.bigImage = PhotoImage(file=IMAGE_DIR+fileName)
        else:
            print(dirName + IMAGE_DIR + fileName + "does not exist.")
            self.bigImage = PhotoImage(file=IMAGE_DIR+"Star_Realms_Sleeve.gif")
        if self.type[0] == "B":
            self.smallImage = PhotoImage(file=BASE_IMAGE_DIR + fileName).subsample(2)
        else:
            self.smallImage = self.bigImage.subsample(2)
        self.primary = card[C_ENUM["PRIMARY"]]
        self.ally = card[C_ENUM["ALLY"]]
        self.scrap = card[C_ENUM["SCRAP"]]
        
        self.abilityDict = deepcopy(self.ally)
        if self.primary != ():
            self.abilityDict["P"] = self.primary
        if self.scrap != ():
            self.abilityDict["$"] = self.scrap
        self.unusedAbilities = deepcopy(self.abilityDict)
        
    def __str__(self):
        return self.name

    def __repr__(self):
        return self.name