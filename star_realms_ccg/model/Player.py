## Star Realms CCG
## Version 2.3.1
##
## This file defines the Player class.
##
## @author: Matthew Milone


from tkinter import PhotoImage

from ..common.helper import drawCards
from ..common.helper import moveCards
from ..common.constants import GUI_IMAGE_DIR
from ..common.constants import HAND_SIZE
from ..common.constants import TRADE_CURVE
from .Base import Base


class Player(object):
    def __init__(self, playerName, playerNumber, tradeDeck, authority, \
                 discardPile, drawPile, field, hand, tradeRow):
        self.srGUI = None
        self.pNum = playerNumber
        self.name = playerName
        if self.name == "":
            self.name = "Player " + str(self.pNum)
        self.opnt = None
        self.tradeRow = tradeRow
        self.tradeCurve = TRADE_CURVE[:]
        if self.pNum == 1:
            self.trBorder = PhotoImage(file=GUI_IMAGE_DIR+"p1_trade_border.gif")
        else:
            self.trBorder = PhotoImage(file=GUI_IMAGE_DIR+"p2_trade_border.gif")
            self.tradeCurve.reverse()
        self.tradeCurve.append(0) # No penalty for Explorers.
        self.authority = authority
        self.trade = 0
        self.combat = 0
        self.tDeck = tradeDeck
        self.discPile = discardPile
        self.drawPile = drawPile
        self.field = field
        self.hand = hand
        self.scrapPile = [] # List of cards scrapped this turn.
        self.numToDisc = 0

    ## Draws cards from draw pile to hand. Shuffles from discard pile if needed.
    def draw(self, n=HAND_SIZE, update=True):
        temp = []
        success = drawCards(self.drawPile, temp, n, backup=self.discPile,
                            backup_name=f"{self.name}'s discard pile", allow_runout=True)
        for card in temp:
            if card.name != card.trueName or card.faction != card.trueFaction:
                card.imitate(card.trueName)
            else:
                card.restoreAbilities()
        moveCards(temp, self.hand, temp[:])
        if update:
            self.srGUI.displayHand()
            self.srGUI.displayField()
            self.srGUI.updateDiscPile()
            self.srGUI.updateDrawLabel()
        if not success:
            self.srGUI.notify(f"{self.name} ran out of cards to draw.")

    ## Returns true if the player has an outpost in their field.
    ## Returns false, otherwise.
    def hasOutposts(self):
        for card in self.field:
            if isinstance(card, Base) and card.isO:
                return True
        return False

    def getActiveBases(self):
        return [card for card in self.field if card.isBase()]

    def getActiveOutposts(self):
        return [card for card in self.field if card.isBase() and card.isO]

    def clearScrapPile(self):
        self.scrapPile = []